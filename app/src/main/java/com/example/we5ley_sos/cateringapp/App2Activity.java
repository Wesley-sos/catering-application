package com.example.we5ley_sos.cateringapp;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.os.Bundle;
import android.widget.Toast;

/**
 * Created by We5ley-sos on 2015-11-16.
 */
public class App2Activity extends Activity {
    Button button;

    @Override
    public void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_to_cart);
    }

    public void onButtonTap (View v){
        Toast myToast = Toast.makeText(getApplicationContext(), "Your request has been saved!", Toast.LENGTH_LONG);
        myToast.show();

    }
}
